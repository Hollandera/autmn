package autmn.core;

import org.lwjgl.Sys;

/**
 * Represents a timing mechanism for a Game.
 *
 * @author inferno04
 */
public class GameTimer {

    /**
     * The time of the last "frame".
     */
    private static long lastFrame;

    /**
     * Construct a GameTimer.
     */
    public GameTimer() {
        lastFrame = getTime();
    }

    /**
     * Get the current time from the high-resolution timer.
     *
     * @return
     */
    public static final long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    /**
     * Get the amount of time that has passed since the last update.
     *
     * @return
     */
    public static final int getDelta() {
        final long time = getTime();
        lastFrame = time;
        return (int) (time - lastFrame);
    }

}
