package autmn.core.window.input.keyboard;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collection;
import java.util.HashSet;

/**
 * Represents a computer Keyboard.
 *
 * @author inferno04
 */
public class Keyboard implements KeyListener {

    private static final Collection<Key> keysDown = new HashSet<Key>();

    /**
     * Respond to a key being typed.
     *
     * @param e
     * @deprecated
     */
    @Deprecated
    @Override
    public void keyTyped(KeyEvent e) {
        // NOT USED
        // keyPressed and keyReleased handle these events
    }

    /**
     * Respond to a key being pressed.
     *
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
        final Key key = new Key(e.getKeyCode());
        keysDown.add(key);
    }

    /**
     * Respond to a key being released.
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        final Key key = new Key(e.getKeyCode());
        keysDown.remove(key);
    }

    /**
     * Return whether the specified Key is currently pressed.
     *
     * @param key
     * @return
     */
    public static boolean isKeyDown(Key key) {
        return keysDown.contains(key);
    }

    /**
     * Register a Keyboard as a KeyListener on a given Component.
     *
     * @param c
     * @return
     */
    public static Keyboard registerAsListener(Component c) {
        final Keyboard k = new Keyboard();
        c.addKeyListener(k);
        return k;
    }
}
