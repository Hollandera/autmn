package autmn.core.window.input.mouse;

public enum MouseButtonState {

    /**
     * Not pressed down
     */
    RELEASED,
    /**
     * Down, but not the "first" time
     */
    PRESSED_DOWN,
    /**
     * Down for the "first" time
     */
    PRESSED_ONCE;
}
