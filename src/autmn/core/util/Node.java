package autmn.core.util;

import autmn.core.util.dataStructures.NList;

public class Node implements Cloneable {

    private NList<Node> subnodes;

    public Node() {
        setSubnodes(new NList<Node>());
    }

    /**
     * Attaches a child.
     *
     * @param node
     */
    public void attach(Node node) {
        getSubnodes().add(node);
    }
    
     /**
     * Detaches a node if it is a direct child.
     *
     * @param node
     */
    public void detach(Node node) {
        getSubnodes().remove(node);
    }
    
     /**
     * Detaches nodes recursively.
     *
     * @param node
     */
    public void deepDetach(Node node) {
        getSubnodes().remove(node);
        for (Node i : subnodes) {
            i.deepDetach(node);
        }
    }

    /**
     * Clear all subnodes
     */
    public void clearSubnodes() {
        subnodes.clear();
    }

    /**
     *
     * @return a list of subnodes.
     */
    public NList<Node> getSubnodes() {
        return subnodes;
    }

    /**
     * set the list sub subenodes
     *
     * @param subnodes
     */
    public void setSubnodes(NList<Node> subnodes) {
        this.subnodes = subnodes;
    }

    /**
     * @return a clone of this node and all of its subnodes.
     */
    public Node clone() {
        Node coppiedNode = new Node();
        for (Node subnode : this.getSubnodes()) {
            coppiedNode.attach(subnode.clone());
        }
        return coppiedNode;
    }
}
