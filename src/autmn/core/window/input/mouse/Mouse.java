package autmn.core.window.input.mouse;

import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Represents a computer mouse.
 *
 * @author inferno04
 */
public class Mouse implements MouseListener, MouseMotionListener {

    private static final int BUTTON_COUNT = MouseInfo.getNumberOfButtons();
    private static final Point polledPosition = new Point(0, 0);
    private static final Point currentPosition = new Point(0, 0);
    private static final MouseButtonState[] mouseButtonStates = new MouseButtonState[BUTTON_COUNT];

    static {
        for (int i = 0; i < BUTTON_COUNT; ++i) {
            mouseButtonStates[i] = MouseButtonState.RELEASED;
        }
    }

    /**
     * Respond to a Mouse button press.
     *
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
        mouseButtonStates[e.getButton() - 1] = MouseButtonState.PRESSED_ONCE;
    }

    /**
     * Respond to a Mouse button release.
     *
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        mouseButtonStates[e.getButton() - 1] = MouseButtonState.RELEASED;
    }

    /**
     * Respond to the Mouse entering Frame.
     *
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        mouseMoved(e);
    }

    /**
     * Respond to the Mouse leaving Frame.
     *
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
        mouseMoved(e);
    }

    /**
     * Respond to the Mouse being dragged.
     *
     * @param e
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        mouseMoved(e);
    }

    /**
     * Respond to the Mouse being moved.
     *
     * @param e
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        currentPosition.setLocation(e.getPoint());
    }

    /**
     * Respond to the Mouse being clicked.
     *
     * @param e
     * @deprecated
     */
    @Deprecated
    @Override
    public void mouseClicked(MouseEvent e) {
        // NOT USED
        // mousePressed and mouseReleased handle these events.
    }

    /**
     * Poll the state of the Mouse.
     */
    public static void pollMouse() {
        // Save the current location
        polledPosition.setLocation(currentPosition);

        // Check each mouse button for 'once' clicks
        for (int i = 0; i < BUTTON_COUNT; i++) {
            if (mouseButtonStates[i] == MouseButtonState.PRESSED_ONCE) {
                mouseButtonStates[i] = MouseButtonState.PRESSED_DOWN;
            }
        }
    }

    /**
     * Return the polled position of the Mouse.
     *
     * @return
     */
    public static Point getPosition() {
        return polledPosition;
    }

    /**
     * Return whether the specified button has been pressed once.
     *
     * @param button
     * @return
     */
    public static boolean isButtonPressedOnce(int button) {
        return mouseButtonStates[button - 1] == MouseButtonState.PRESSED_ONCE;
    }

    /**
     * Return whether the specified button is down.
     *
     * @param button
     * @return
     */
    public static boolean isButtonDown(int button) {
        return mouseButtonStates[button - 1] == MouseButtonState.PRESSED_DOWN;
    }

    /**
     * Return the number of buttons present on the Mouse.
     *
     * @return
     */
    public static int getNumberOfButtons() {
        return BUTTON_COUNT;
    }

    /**
     * Register a Mouse as a MouseListener and MouseMotionListener of the
     * specified Component.
     *
     * @param c
     * @return
     */
    public static Mouse registerAsListener(Component c) {
        final Mouse m = new Mouse();
        c.addMouseListener(m);
        c.addMouseMotionListener(m);
        return m;
    }
}
