package autmn.core.util.exceptions;

/**
 * Represents an Exception occurring within Autmn.
 *
 * @author william gervasio
 */
public class AutmnException extends Exception {

    /**
     * Construct a context-less AutmnException
     */
    public AutmnException() {
        super();
    }

    /**
     * Construct an AutmnException with a stacktrace message.
     * @param message
     */
    public AutmnException(String message) {
        super(message);
    }

    /**
     * Construct an AutmnException with a cause.
     * @param cause
     */
    public AutmnException(Throwable cause) {
        super(cause);
    }

    /**
     * Construct an AutmnException with a stacktrace message and a cause.
     * @param message
     * @param cause
     */
    public AutmnException(String message, Throwable cause) {
        super(message, cause);
    }
}
