package autmn.core.scene;

public interface Updateable {
	public abstract void update(float delta);
}
