package autmn.core.util.dataStructures;

import java.util.LinkedList;

public class NList<E> extends LinkedList<E> {

    public NList() {

    }

    public void pushDown(int index) {
        if (index > 0 && index <  super.size()) {
            E placeHolder = super.get(index);
            super.set(index, super.get(index - 1));
            super.set(index - 1, placeHolder);
        }
    }

    public void pullUp(int index) {
        if (index >= 0 && index < super.size() - 1) {
            E placeHolder = super.get(index);
            super.set(index, super.get(index + 1));
            super.set(index + 1, placeHolder);
        }
    }
}
