package autmn.game;

import autmn.core.Application;
import autmn.core.util.math.Vector2;

public class Main {
	public static void main(String[] args) {
		Vector2 v1 = new Vector2 (5, 5);
		Vector2 v2 = v1.clone();
		v2.set(1f, 1f);
		System.out.println(v1.x);
		System.out.println(v2.y);
		Application  a = new Application(new TestGame(),"test", 360, 420, 3, 2);
	}
}
