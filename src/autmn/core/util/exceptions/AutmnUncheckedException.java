package autmn.core.util.exceptions;

/**
 * Represents an unchecked Exception occurring within Autmn.
 *
 * @author inferno04
 */
public class AutmnUncheckedException extends RuntimeException {

    /**
     * Construct a context-less AutmnUncheckedException.
     */
    public AutmnUncheckedException() {
        super();
    }

    /**
     * Construct an AutmnUncheckedException with a stacktrace message.
     *
     * @param message
     */
    public AutmnUncheckedException(String message) {
        super(message);
    }

    /**
     * Construct an AutmnUncheckedException with a cause.
     *
     * @param cause
     */
    public AutmnUncheckedException(Throwable cause) {
        super(cause);
    }

    /**
     * Construct an AutmnUncheckedException with a stacktrace message and a
     * cause.
     *
     * @param message
     * @param cause
     */
    public AutmnUncheckedException(String message, Throwable cause) {
        super(message, cause);
    }

}
