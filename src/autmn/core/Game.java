package autmn.core;

public abstract class Game {

    Screen screen;

    public Game() {
        screen = null;
    }

    public abstract void init();

    public abstract void update(int delta);

    public abstract void render(int delta);

    public void swapScreens(Screen screen) throws Exception {
        this.screen.leave();
        this.screen = screen;
        this.screen.resume();
    }
}
