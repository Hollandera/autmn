/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autmn.core.util;

/**
 *
 * @author inferno04
 */
public class StringUtil {

    public static String toString(Object obj, String strContent) {
        String toString = obj.getClass().getCanonicalName() + ": " + strContent;
        return toString;
    }

    public static String toString(Object... objs) {
        String toString = "[";
        int i;
        for (i = 0; i < objs.length - 1; i++) {
            toString += objs[i].toString() + ", ";
        }
        toString += objs[i] + "]";
        return toString;
    }

}
