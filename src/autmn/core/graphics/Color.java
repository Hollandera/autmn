/*
 *	Bugs:
 * 		~clamp does not deal with negatives
 */
package autmn.core.graphics;

import autmn.core.util.MathUtil;
import autmn.core.util.StringUtil;

/**
 *
 * @author william gervasio
 *
 */
public class Color implements Cloneable {

    public static final float MINIMUM_CHANNEL_VALUE = 0.0f;
    public static final float MAXIMUM_CHANNEL_VALUE = 1.0f;

    private float r, g, b, a;

    /**
     *
     */
    public Color() {
        this(MINIMUM_CHANNEL_VALUE, MINIMUM_CHANNEL_VALUE, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
    }

    /**
     *
     * @param r
     * @param g
     * @param b
     * @param a
     */
    public Color(float r, float g, float b, float a) {
        set(r, g, b, a);
    }

    /**
     *
     * @param percentage
     * @return
     */
    public Color brighten(float percentage) {
        percentage += 1;

        r *= percentage;
        g *= percentage;
        b *= percentage;

        return clamp();
    }

    /**
     *
     * @param percentage
     * @return
     */
    public Color darken(float percentage) {
        percentage = 1 - percentage;

        r *= percentage;
        g *= percentage;
        b *= percentage;

        return clamp();
    }

    /**
     *
     * @param r
     * @param g
     * @param b
     * @param a
     * @return
     */
    public final Color set(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;

        return clamp();
    }

    /**
     *
     * @param r
     * @return
     */
    public Color setRed(float r) {
        this.r = MathUtil.ensureWithinRange(r, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
        return this;
    }

    /**
     *
     * @return
     */
    public float getRed() {
        return r;
    }

    /**
     *
     * @param g
     * @return
     */
    public Color setGreen(float g) {
        this.g = MathUtil.ensureWithinRange(g, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
        return this;
    }

    /**
     *
     * @return
     */
    public float getGreen() {
        return g;
    }

    /**
     *
     * @param b
     * @return
     */
    public Color setBlue(float b) {
        this.b = MathUtil.ensureWithinRange(b, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
        return this;
    }

    /**
     *
     * @return
     */
    public float getBlue() {
        return b;
    }

    /**
     *
     * @param a
     * @return
     */
    public Color setAlpha(float a) {
        this.a = MathUtil.ensureWithinRange(a, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
        return this;
    }

    /**
     *
     * @return
     */
    public float getAlpha() {
        return a;
    }

    /**
     *
     * @return
     */
    public float[] getElements() {
        return new float[]{r, g, b, a};
    }

    /**
     * @return
     */
    @Override
    public Color clone() {
        return new Color(r, g, b, a);
    }

    /**
     *
     * @return An instance of this object
     */
    private Color clamp() {
        r = MathUtil.ensureWithinRange(r, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
        g = MathUtil.ensureWithinRange(g, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
        b = MathUtil.ensureWithinRange(b, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);
        a = MathUtil.ensureWithinRange(a, MINIMUM_CHANNEL_VALUE, MAXIMUM_CHANNEL_VALUE);

        return this;
    }

    @Override
    public String toString() {
        String colorValues = StringUtil.toString(r, g, b, a);
        String toString = StringUtil.toString(this, colorValues);
        return toString;
    }
}
