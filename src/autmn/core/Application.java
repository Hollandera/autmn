package autmn.core;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

public class Application {

    private Game game;

    public Application(Game game, String title, int width, int height, int oglMajor, int oglMinor) {
        this.game = game;
        initLWJGL(title, width, height, oglMajor, oglMinor);
        start();
    }

    public final void initLWJGL(String title, int width, int height, int oglMajor,
            int oglMinor) {

        try {
            PixelFormat pixelFormat = new PixelFormat();
            ContextAttribs contextAtrributes = new ContextAttribs(3, 2)
                    .withForwardCompatible(true)
                    .withProfileCore(true);

            Display.setDisplayMode(new DisplayMode(width, height));
            Display.setTitle(title);
            Display.create(pixelFormat, contextAtrributes);

            GL11.glViewport(0, 0, width, height);
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        // Setup an XNA like background color
        GL11.glClearColor(0.4f, 0.6f, 0.9f, 0f);

        // Map the internal OpenGL coordinate system to the entire screen
        GL11.glViewport(0, 0, width, height);
    }

    public final void start() {
        game.init();
        while (!Display.isCloseRequested()) {
            // Do a single loop (logic/render)
            final int delta = GameTimer.getDelta();
            game.update(delta);

            // Force a maximum FPS of about 60
            Display.sync(60);
            // Let the CPU synchronize with the GPU if GPU is tagging behind
            Display.update();
        }
    }
}
