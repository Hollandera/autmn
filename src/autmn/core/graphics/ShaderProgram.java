/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autmn.core.graphics;

import autmn.core.util.exceptions.AutmnException;
import autmn.core.util.AutmnLog;
import autmn.core.util.resource.Resource;
import java.util.Map;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL32;

/**
 *
 * @author Will
 */
public class ShaderProgram implements Resource {

    private int handle;

    /**
     *
     * @param vert
     * @param frag
     * @throws AutmnException
     */
    public ShaderProgram(String vertexShader, String fragmentShader, Map<Integer, String> attributes) {
        int vertexHandle, fragmentHandle;
        
        try {
            vertexHandle = compileShader(vertexShader, GL20.GL_VERTEX_SHADER);
            fragmentHandle = compileShader(fragmentShader, GL20.GL_FRAGMENT_SHADER);
        } catch(AutmnException e) {    
            AutmnLog.log(e.getMessage());
            return;
        }

        handle = GL20.glCreateProgram();

        GL20.glAttachShader(handle, vertexHandle);
        GL20.glAttachShader(handle, fragmentHandle);

        if (attributes != null) {
            for (Map.Entry<Integer, String> e : attributes.entrySet()) {
                GL20.glBindAttribLocation(handle, e.getKey(), e.getValue());
            }
        }

        GL20.glLinkProgram(handle);

        if (GL20.glGetProgrami(handle, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
            AutmnLog.log("Failed to link shader");
            return;
        }

        GL20.glDetachShader(handle, vertexHandle);
        GL20.glDetachShader(handle, fragmentHandle);
        GL20.glDeleteShader(vertexHandle);
        GL20.glDeleteShader(fragmentHandle);
    }

    /**
     *
     * @param vert
     * @param frag
     * @param geom
     * @throws AutmnException
     */
    public ShaderProgram(String vertexShader, String fragmentShader, String geometryShader, Map<Integer, String> attributes) {
        int vertexHandle, fragmentHandle, geometryHandle;
        
        try {
            vertexHandle = compileShader(vertexShader, GL20.GL_VERTEX_SHADER);
            fragmentHandle = compileShader(fragmentShader, GL20.GL_FRAGMENT_SHADER);
            geometryHandle = compileShader(geometryShader, GL32.GL_GEOMETRY_SHADER);
        } catch(AutmnException e) {    
            AutmnLog.log(e.getMessage());
            return;
        }

        handle = GL20.glCreateProgram();

        GL20.glAttachShader(handle, vertexHandle);
        GL20.glAttachShader(handle, fragmentHandle);
        GL20.glAttachShader(handle, geometryHandle);

        if (attributes != null) {
            for (Map.Entry<Integer, String> e : attributes.entrySet()) {
                GL20.glBindAttribLocation(handle, e.getKey(), e.getValue());
            }
        }

        GL20.glLinkProgram(handle);

        if (GL20.glGetProgrami(handle, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
            AutmnLog.log("Failed to link shader");
            return;
        }

        GL20.glDetachShader(handle, vertexHandle);
        GL20.glDetachShader(handle, fragmentHandle);
        GL20.glDetachShader(handle, geometryHandle);
        GL20.glDeleteShader(vertexHandle);
        GL20.glDeleteShader(fragmentHandle);
        GL20.glDeleteShader(geometryHandle);
    }

    /**
     *
     */
    public void bind() {
        GL20.glUseProgram(handle);
    }

    /**
     *
     */
    public void unbind() {
        GL20.glUseProgram(0);
    }

    /**
     *
     */
    public void destroy() {
        GL20.glDeleteProgram(handle);
    }

    /**
     *
     * @param str
     * @return
     */
    public int getUniformLocation(String string) {
        return GL20.glGetUniformLocation(handle, string);
    }

    /**
     *
     * @param shader
     * @param type
     * @return
     * @throws AutmnException
     */
    private int compileShader(String shader, int type) throws AutmnException {
        int handle = GL20.glCreateShader(type);
        GL20.glShaderSource(handle, shader);
        GL20.glCompileShader(handle);

        if (GL20.glGetShaderi(handle, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            switch(type) {
                case GL20.GL_VERTEX_SHADER:
                    throw new AutmnException("Failed to compiling vertex shader");
                case GL20.GL_FRAGMENT_SHADER:
                    throw new AutmnException("Failed to compiling fragment shader");
                case GL32.GL_GEOMETRY_SHADER:
                    throw new AutmnException("Failed to compiling geometry shader");
            }   
        }

        return handle;
    }
}
