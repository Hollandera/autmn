/*
 * Status: finalized
 */
package autmn.core.graphics.opengl;

import org.lwjgl.opengl.GL11;

public class Descriptor {

	private final int size;
	private final int type;
	private final boolean normalized;
	private final int stride;
	private final int pointer;

	public Descriptor(final int size, final int type, final boolean normalized,
			final int stride, final int pointer) {
		this.size = size;
		this.type = type;
		this.normalized = normalized;
		this.stride = stride;
		this.pointer = pointer;
	}
        
        public static Descriptor createPositionDescriptor(int vertexSize, int offset) {
            return new Descriptor(3, GL11.GL_FLOAT, false, vertexSize, offset);
        }
        
        public static Descriptor createColorDescriptor(int vertexSize, int offset) {
            return new Descriptor(4, GL11.GL_FLOAT, false, vertexSize, offset);
        }
        
        public static Descriptor createNormalDescriptor(int vertexSize, int offset) {
            return new Descriptor(3, GL11.GL_FLOAT, false, vertexSize, offset);
        }
        
        public static Descriptor createTexcoordDescriptor(int vertexSize, int offset) {
            return new Descriptor(2, GL11.GL_FLOAT, false, vertexSize, offset);
        }

	public int getSize() {
		return size;
	}

	public int getType() {
		return type;
	}

	public boolean isNormalized() {
		return normalized;
	}

	public int getStride() {
		return stride;
	}

	public int getPointer() {
		return pointer;
	}
}
