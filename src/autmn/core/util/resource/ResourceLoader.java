package autmn.core.util.resource;

import java.io.IOException;

public abstract class ResourceLoader {
	public abstract Resource load(String fileLocation) throws IOException;
}
