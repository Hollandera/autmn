package autmn.core.scene;

public interface Renderable {
	public abstract void render(int delta);
}
