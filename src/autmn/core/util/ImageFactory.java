package autmn.core.util;

import autmn.core.graphics.Image;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import autmn.core.util.resource.Resource;
import autmn.core.util.resource.ResourceManager;
import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

/**
 * 
 * @author william
 *
 */
public class ImageFactory {
	
	/**
	 * Loads the image and passes it to the resource manager.
	 * @param id
	 * @param fileLocation
	 * @return A reference to the image.
	 * @throws IOException
	 */
	public Image load(int id, String fileLocation) {
		int width, height;
		ByteBuffer buffer;
		try {
			InputStream in = new FileInputStream(fileLocation);
			PNGDecoder decoder = new PNGDecoder(in);

			width = decoder.getWidth();
			height = decoder.getHeight();
			
			int bytesOfMemoryToAllocate = 4 * decoder.getWidth() * decoder.getHeight();
			buffer = ByteBuffer.allocateDirect(bytesOfMemoryToAllocate);
			decoder.decode(buffer, decoder.getWidth() * 4, Format.RGBA);
			in.close();
			buffer.flip();
		} catch (IOException e) {
			System.out.println("Failed to load image");
			return null;
		}
		
		return create(id, width, height, buffer);
	}
	
	/**
	 * 
	 * @param id
	 * @param width
	 * @param height
	 * @param buffer
	 * @return
	 */
	public Image create(int id, int width, int height, ByteBuffer buffer) {
		Image image = new Image(width, height, buffer);
		ResourceManager.add(id, image);
		return image;
	}
}
