package autmn.core.window.input.keyboard;

import java.awt.event.KeyEvent;

/**
 * Holds commonly used Keyboard Keys.
 *
 * @author inferno04
 */
public final class Keys {

    private Keys() {
        // Static class. No instantiation.
    }

    public static final Key ESC = new Key(KeyEvent.VK_ESCAPE);
    public static final Key F1 = new Key(KeyEvent.VK_F1);
    public static final Key F2 = new Key(KeyEvent.VK_F2);
    public static final Key F3 = new Key(KeyEvent.VK_F2);
    public static final Key F4 = new Key(KeyEvent.VK_F2);
    public static final Key F5 = new Key(KeyEvent.VK_F2);
    public static final Key F6 = new Key(KeyEvent.VK_F2);
    public static final Key F7 = new Key(KeyEvent.VK_F2);
    public static final Key F8 = new Key(KeyEvent.VK_F2);
    public static final Key F9 = new Key(KeyEvent.VK_F2);
    public static final Key F10 = new Key(KeyEvent.VK_F2);
    public static final Key F11 = new Key(KeyEvent.VK_F2);
    public static final Key F12 = new Key(KeyEvent.VK_F2);
    public static final Key INSERT = new Key(KeyEvent.VK_F2);
    public static final Key PRINT_SCREEN = new Key(KeyEvent.VK_F2);
    public static final Key DELETE = new Key(KeyEvent.VK_F2);

    public static final Key TILDE = new Key(KeyEvent.VK_DEAD_TILDE);

    public static final Key UP = new Key(KeyEvent.VK_UP);
    public static final Key DOWN = new Key(KeyEvent.VK_DOWN);
    public static final Key LEFT = new Key(KeyEvent.VK_LEFT);
    public static final Key RIGHT = new Key(KeyEvent.VK_RIGHT);

}
