package autmn.game;

import autmn.core.Game;
import autmn.core.graphics.ShaderProgram;
import autmn.core.graphics.opengl.BufferedObjectUsage;
import autmn.core.graphics.opengl.Descriptor;
import autmn.core.graphics.opengl.IBO;
import autmn.core.graphics.opengl.VAO;
import autmn.core.graphics.opengl.VBO;
import autmn.core.util.FileUtil;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

public class TestGame extends Game {

    VAO vao;

    public TestGame() {

    }

    public void init() {
        setupShaders();
        float[] vertices = {
            -0.5f, 0.5f, 0f,
            -0.5f, -0.5f, 0f,
            0.5f, -0.5f, 0f,
            0.5f, 0.5f, 0f,
        };
        
        
        float[] colors = {
		1f, 0f, 0f, 1f,
		0f, 1f, 0f, 1f,
		0f, 0f, 1f, 1f,
		1f, 1f, 1f, 1f,
        };
        
        FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length + colors.length);
        buffer.put(vertices);
        buffer.put(colors);
        buffer.flip();
        
        int[] indices = {0, 1, 2, 0, 2, 3,};

        IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indices.length);
        indicesBuffer.put(indices);
        indicesBuffer.flip();

        IBO ibo = new IBO(indicesBuffer, BufferedObjectUsage.STATIC_DRAW);
        VBO vbo = new VBO(buffer, BufferedObjectUsage.STATIC_DRAW);
        vao = new VAO(vbo, ibo, 6);
        Descriptor des = new Descriptor(3, GL11.GL_FLOAT, false, 0, 0);
        vao.addVertexAttribute(0, des);
        //vao.addVertexAttribute(1, Descriptor.colorDescriptor());
        vao.init();
    }
    /*
     private void setupQuad() {
     // We'll define our quad using 4 vertices of the custom 'TexturedVertex' class
     TexturedVertex v0 = new TexturedVertex();
     v0.setXYZ(-0.5f, 0.5f, 0); v0.setRGB(1, 0, 0); v0.setST(0, 0);
     TexturedVertex v1 = new TexturedVertex();
     v1.setXYZ(-0.5f, -0.5f, 0); v1.setRGB(0, 1, 0); v1.setST(0, 1);
     TexturedVertex v2 = new TexturedVertex();
     v2.setXYZ(0.5f, -0.5f, 0); v2.setRGB(0, 0, 1); v2.setST(1, 1);
     TexturedVertex v3 = new TexturedVertex();
     v3.setXYZ(0.5f, 0.5f, 0); v3.setRGB(1, 1, 1); v3.setST(1, 0);
		
     TexturedVertex[] vertices = new TexturedVertex[] {v0, v1, v2, v3};
     // Put each 'Vertex' in one FloatBuffer
     FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(vertices.length *
     TexturedVertex.elementCount);
     for (int i = 0; i < vertices.length; i++) {
     // Add position, color and texture floats to the buffer
     verticesBuffer.put(vertices[i].getElements());
     }
     verticesBuffer.flip();
     // OpenGL expects to draw vertices in counter clockwise order by default
     byte[] indices = {
     0, 1, 2,
     2, 3, 0
     };
     indicesCount = indices.length;
     ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indicesCount);
     indicesBuffer.put(indices);
     indicesBuffer.flip();

     // Create a new Vertex Array Object in memory and select it (bind)
     vaoId = GL30.glGenVertexArrays();
     GL30.glBindVertexArray(vaoId);
		
     // Create a new Vertex Buffer Object in memory and select it (bind)
     vboId = GL15.glGenBuffers();
     GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
     GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW);

     // Put the position coordinates in attribute list 0
     GL20.glVertexAttribPointer(0, TexturedVertex.positionElementCount, GL11.GL_FLOAT,
     false, TexturedVertex.stride, TexturedVertex.positionByteOffset);
     // Put the color components in attribute list 1
     GL20.glVertexAttribPointer(1, TexturedVertex.colorElementCount, GL11.GL_FLOAT,
     false, TexturedVertex.stride, TexturedVertex.colorByteOffset);
     // Put the texture coordinates in attribute list 2
     GL20.glVertexAttribPointer(2, TexturedVertex.textureElementCount, GL11.GL_FLOAT,
     false, TexturedVertex.stride, TexturedVertex.textureByteOffset);

     GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		
     // Deselect (bind to 0) the VAO
     GL30.glBindVertexArray(0);

     // Create a new VBO for the indices and select it (bind) - INDICES
     vboiId = GL15.glGenBuffers();
     GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
     GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
     GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

     this.exitOnGLError("setupQuad");
     }
     */

    private void setupShaders() {
        Map<Integer, String> attributes = new HashMap<Integer, String>();
        attributes.put(0, "in_Position");
        attributes.put(1, "in_Color");
        attributes.put(2, "in_TextureCoord");

        ShaderProgram s = new ShaderProgram(FileUtil.readText("res/BasicShader.vert"), FileUtil.readText("res/BasicShader.frag"), attributes);
    }

    @Override
    public void update(int delta) {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
        vao.draw();
    }

    @Override
    public void render(int delta) {
    }
}
