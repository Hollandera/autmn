package autmn.core.util.math;

import autmn.core.graphics.Elements;

/**
 *
 * @author william gervasio
 *
 */
public class Vector3 implements Cloneable, Elements<Float> {

    public static final Vector3 LEFT, RIGHT, UP, DOWN, ZERO, FORWARD, BACKWARD;

    static {
        ZERO = new Vector3(0, 0, 0);
        LEFT = new Vector3(-1, 0, 0);
        RIGHT = new Vector3(1, 0, 0);
        UP = new Vector3(0, 1, 0);
        DOWN = new Vector3(0, -1, 0);
        FORWARD = new Vector3(0, 0, 1);
        BACKWARD = new Vector3(0, 0, -1);
    }
    public float x, y, z;

    /**
     *
     */
    public Vector3() {
        set(0, 0, 0);
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     */
    public Vector3(float x, float y, float z) {
        set(x, y, z);
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public Vector3 set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;

        return this;
    }

    /**
     *
     * @param other
     * @return
     */
    public Vector3 set(Vector3 other) {
        return set(other.x, other.y, other.z);
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public Vector3 add(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;

        return this;
    }

    /**
     *
     * @param other
     * @return
     */
    public Vector3 add(Vector3 other) {
        return add(other.x, other.y, other.z);
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public Vector3 sub(float x, float y, float z) {
        this.x -= x;
        this.y -= y;
        this.z -= z;

        return this;
    }

    /**
     *
     * @param other
     * @return
     */
    public Vector3 sub(Vector3 other) {
        return sub(other.x, other.y, other.z);
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public Vector3 mul(float x, float y, float z) {
        this.x *= x;
        this.y *= y;
        this.z *= z;

        return this;
    }

    /**
     *
     * @param other
     * @return
     */
    public Vector3 mul(Vector3 other) {
        return mul(other.x, other.y, other.z);
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public Vector3 div(float x, float y, float z) {
        this.x /= x;
        this.y /= y;
        this.z /= z;

        return this;
    }

    /**
     *
     * @param other
     * @return
     */
    public Vector3 div(Vector3 other) {
        return div(other.x, other.y, other.z);
    }

    public Vector3 scale(float scalar) {
        return mul(scalar, scalar, scalar);
    }

    /**
     *
     * @param other
     * @return
     */
    public float dot(Vector3 other) {
        return (x * other.x) + (y * other.y + (z * other.z));
    }

    /**
     *
     * @return
     */
    public float length() {
        return (float) Math.sqrt(length2());
    }

    /**
     *
     * @return
     */
    public float length2() {
        return (float) (x * x + y * y + z * z);
    }

    /**
     *
     * @return
     */
    public Vector3 invert() {
        return set(-x, -y, -z);
    }

    /**
     *
     * @return
     */
    public Vector3 inverse() {
        return clone().invert();
    }

    /**
     *@return
     */
    @Override
    public Vector3 clone() {
        return new Vector3(x, y, z);
    }

    /**
     *
     * @return
     */
    @Override
    public Float[] getElements() {
        return new Float[]{x, y, z};
    }
}
