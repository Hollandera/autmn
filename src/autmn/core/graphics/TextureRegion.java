/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autmn.core.graphics;

import autmn.core.util.math.Vector2;

/**
 *
 * @author Will
 */
public class TextureRegion implements Cloneable {

    public float s1, t1, s2, t2;

    public TextureRegion() {
        this.s1 = 0;
        this.t1 = 0;
        this.s2 = 1;
        this.t2 = 1;
    }
        
    public TextureRegion(float s1, float t1, float s2, float t2) {
        this.s1 = s1;
        this.t1 = t1;
        this.s2 = s2;
        this.t2 = t2;
    }

    public TextureRegion(Vector2 coord1, Vector2 coord2) {
        this(coord1.x, coord1.y, coord2.x, coord2.y);
    }

    public TextureRegion getFlipped(boolean flipX, boolean flipY) {
        TextureRegion flippedRegion = clone();
        if(flipX) {
            float temp = flippedRegion.s1;
            flippedRegion.s1 = s2;
            flippedRegion.s2 = temp;
        }
        if(flipY) {
            float temp = flippedRegion.t1;
            flippedRegion.t1 = t2;
            flippedRegion.t2 = temp;
        }
        
        return flippedRegion;
    }

    @Override
    public TextureRegion clone() {
        return new TextureRegion(s1, t1, s2, t2);
    }
}
