/*
 * Index Buffer Object
 * Status: finalized
 */
package autmn.core.graphics.opengl;

import java.nio.IntBuffer;

import org.lwjgl.opengl.GL15;

public class IBO {
	private final int handle;
	
	/**
	 * 
	 * @param buffer
	 * @param usage
	 */
	public IBO(IntBuffer buffer, int usage) {
		handle = GL15.glGenBuffers();

		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, handle);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer,
				usage);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	
	/**
	 * 
	 */
	public void bind() {
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, handle);
	}
	
	/**
	 * 
	 */
	public void destroy() {
		GL15.glDeleteBuffers(handle);
	}
}
