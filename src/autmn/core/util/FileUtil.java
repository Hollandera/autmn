/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autmn.core.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Will
 */
public class FileUtil {

    public static String readText(String fileLocation) {
        StringBuilder shaderString = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileLocation));
            String line;
            while ((line = reader.readLine()) != null) {
                shaderString.append(line).append("\n");
            }
            reader.close();
        } catch (FileNotFoundException e) {
            AutmnLog.log("Failed to load file " + fileLocation + ". File could not be found");
        } catch (IOException e) {
            AutmnLog.log("Failed to load file " + fileLocation + ". An IO exception occured");
        }
        
        return shaderString.toString();
    }
}
