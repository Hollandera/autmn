/*
 *	Vertex Buffer Object
 *	Status: Finalized
 */
package autmn.core.graphics.opengl;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL15;

public class VBO {
    private final int handle;

    public VBO(FloatBuffer buffer, int usage) {
        handle = GL15.glGenBuffers();

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, handle);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, usage);
    }

    public void bind() {
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, handle);
    }

    public void destroy() {
        GL15.glDeleteBuffers(handle);
    }
}
