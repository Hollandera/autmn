package autmn.core.window.input.keyboard;

/**
 * Represents a key on a keyboard. The Key class is effectively an Integer. Used
 * for a more friendly and intuitive API. The Key class also provides some
 * commonly used keys as static members for easy reference.
 *
 * @author inferno04
 */
public class Key {

    /**
     * The Java virtual-keyboard keycode for this Key.
     */
    public final int keycode;

    /**
     * Construct a Key with the given keycode.
     *
     * @param keycode
     */
    public Key(int keycode) {
        this.keycode = keycode;
    }

    /**
     * Return the keycode (as defined by KeyEvent, hopefully) of this Key.
     *
     * @return
     */
    public int getKeycode() {
        return keycode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Key) {
            Key k = (Key) obj;
            return k.keycode == keycode;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.keycode;
        return hash;
    }
}
