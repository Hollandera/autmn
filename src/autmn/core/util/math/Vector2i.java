package autmn.core.util.math;

public class Vector2i implements Cloneable {

    public int x, y;

    public Vector2i() {
        this(0, 0);
    }

    public Vector2i(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector2i invert() {
        return set(-x, -y);
    }

    public Vector2i inverse() {
        return clone().invert();
    }

    public Vector2i set(int x, int y) {
        this.x = x;
        this.y = x;
        return this;
    }

    public Vector2i add(int x, int y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public Vector2i add(Vector2i other) {
        return add(other.x, other.y);
    }

    public Vector2i sub(int x, int y) {
        this.x -= x;
        this.y -= y;
        return this;
    }

    public Vector2i sub(Vector2i other) {
        return sub(other.x, other.y);
    }

    public Vector2i mul(int x, int y) {
        this.x *= x;
        this.y *= y;
        return this;
    }

    public Vector2i mul(Vector2i other) {
        return mul(other.x, other.y);
    }

    public Vector2i mul(int scalar) {
        return mul(scalar, scalar);
    }

    public Vector2i div(int x, int y) {
        this.x /= x;
        this.y /= y;
        return this;
    }

    public Vector2i div(Vector2i other) {
        return div(other.x, other.y);
    }

    public Vector2i div(int scalar) {
        return div(scalar, scalar);
    }

    public float dot(Vector2i other) {
        return (x * other.y) + (y * other.y);
    }

    public float len() {
        return (int) Math.sqrt(len2());
    }

    public float len2() {
        return (float) (Math.pow(x, 2) + Math.pow(y, 2));
    }

    public float dist(int x, int y) {
        return (float) Math.sqrt(dist2(x, y));
    }

    public float dist(Vector2i other) {
        return dist(other.x, other.y);
    }

    public int dist2(int x, int y) {
        final int x_d = x - x;
        final int y_d = y - y;
        return x_d * x_d + y_d * y_d;
    }

    public float dist2(Vector2i other) {
        return dist2(other.x, other.y);
    }

    public float angle(Vector2i other) {
        float v = len();
        float w = other.len();

        return (float) Math.acos(dot(other) / (v * w));
    }

    public Vector2i clone() {
        return new Vector2i(x, y);
    }
}
