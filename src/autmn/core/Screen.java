package autmn.core;

public interface Screen 
{
	public abstract void pause();
	public abstract void resume();
	public abstract void leave();
	public abstract void resize(int width, int height);
	public abstract void render(int delta);
	public abstract void update(int delta);
}
