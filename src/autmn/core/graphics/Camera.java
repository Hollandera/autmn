package autmn.core.graphics;

import autmn.core.util.math.Vector3;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;

public abstract class Camera {
    private Quaternion rotation;
    private Vector3 position;
    private Matrix4f view, projection, combined;
    
    public Camera() {
        view = new Matrix4f();
        projection = new Matrix4f();
    }
    
    public void pitch(float amount) {
        
    }
    
    public void yaw(float amount) {
        
    }
    
    public void roll(float amount) {
        
    }

    public Vector3 getPosition() {
        
    }
    
    public Vector3 getRotation() {
        
    }

}
