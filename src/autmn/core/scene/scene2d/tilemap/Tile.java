/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autmn.core.scene.scene2d.tilemap;

import autmn.core.graphics.TextureRegion;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Will
 */
public class Tile {

    private Map<String, String> properties = new HashMap<String, String>();
    private TextureRegion texture;

    public Tile(TextureRegion texture) {
        this.texture = texture;
    }

    public void addProperty(String property, String value) {
        properties.put(property, value);
    }

    public boolean hasProperty(String property) {
        return properties.containsKey(property);
    }

    public String getProperty(String property) {
        return properties.get(property);
    }

    public TextureRegion getTexture() {
        return texture;
    }
}
