package autmn.core.util;

public class Stat {
	
	private int current;
	private int max;
	private int modifier;
	
	public Stat(int max){
		this.current = this.max = max;
		this.modifier = 0;
	}
	public Stat(int current, int max){
		this.current = current;
		this.max = max;
		this.modifier = 0;
	}
	public void addCurrent(int ammount){
		this.current += ammount;
		if(this.current > this.max)
			this.setCurrent(this.max);
	}
	public void subCurrent(int ammount){
		this.current -= ammount;
	}
	public void setCurrent(int ammount){
		this.current = ammount;
	}
	public void addMax(int ammount){
		this.max += ammount;
	}
	public void subMax(int ammount){
		this.max -= ammount;
	}
	public void setMax(int ammount){
		this.max = ammount;
	}
	public void addModifier(int ammount){
		this.modifier += ammount;
	}
	public void subModifier(int ammount){
		this.modifier -= ammount;
	}
	public void setModifier(int ammount){
		this.modifier = ammount;
	}
	public void resetModifier(){
		modifier = 0;
	}
	public int getCurrent(){
		return current;
	}
	public int getMax(){
		return max;
	}
	public int getModifier(){
		return modifier;
	}
}
