/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autmn.core.graphics;

import autmn.core.util.resource.Resource;
import java.nio.ByteBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;

public class Texture implements Resource {

    public static final int UNIT_DIFFUSE = GL13.GL_TEXTURE0;
    public static final int BEHAVIOR_REPEAT = GL11.GL_REPEAT;
    public static final int BEHAVIOR_CLAMP = GL12.GL_CLAMP_TO_EDGE;

    private final int handle, unit, width, height;

    /**
     *
     * @param width The texture width in pixels
     * @param height The texture height in pixels
     * @param buffer 
     * @param unit
     * @param behavior
     */
    public Texture(int width, int height, ByteBuffer buffer, int unit, int behavior) {
        this.unit = unit;
        this.width = width;
        this.height = height;

        handle = GL11.glGenTextures();

        GL13.glActiveTexture(unit);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, handle);

        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, behavior);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, behavior);

        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width,
                height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

    }

    /**
     *
     */
    public void bind() {
        GL13.glActiveTexture(unit);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, handle);
    }

    /**
     *
     */
    public void destroy() {
        GL11.glDeleteTextures(handle);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
