/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autmn.core.scene.scene2d;

import autmn.core.util.math.Vector2;

/**
 *
 * @author Will
 */
public abstract class SceneObject {
    private Vector2 translation, scale;
    public SceneObject() {
        
    }
    
    public void setScale(Vector2 scale) {
        this.scale = scale;
    }
    
    public SceneObject translate(float x, float y) {
        translation.add(x, y);
        return this;
    }
    
    public SceneObject translate(Vector2 distance) {
        translation.add(distance);
        return this;
    }
    
    public SceneObject setTranslation(float x, float y) {
        this.translation.set(x, y);
        return this;
    }
    
    public SceneObject setTranslation(Vector2 translation) {
        this.translation = translation;
        return this;
    }
}
