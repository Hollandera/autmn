/*
 * NOTE: not TESTED!!!!!!
 */

package autmn.core.util;

import org.lwjgl.Sys;
/**
 *
 * @author Will
 */
public class Timer {
    
    public long startTime, stopTime, totalTime;
    public boolean running;
    public Timer() {
        startTime = 0;
        stopTime = 0;
        totalTime = 0;
        running = false;
    }
    
    public void start() {
        if(!running) {
            running = true;
            startTime = getCurrentTimeMS();
        }
    }
    
    public void stop() {
        if(running) {
            running = false;
            stopTime = getCurrentTimeMS();
            totalTime += stopTime - startTime;
        }
    }
    
    public void reset() {
        long time = getCurrentTimeMS();
        startTime = time;
        stopTime = time;
        totalTime = 0;
    }
    
    public long getElapsedTimeMS() {
        if(running) {
            return getCurrentTimeMS() - startTime + totalTime;
        }

        return totalTime;
    }
    
    public long getElapsedTimeSec() {
        return (long)Math.floor(getElapsedTimeMS() / 1000);
    }
    
    public static long getCurrentTimeMS() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }
}
