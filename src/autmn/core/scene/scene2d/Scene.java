/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autmn.core.scene.scene2d;

import autmn.core.scene.Renderable;
import autmn.core.scene.Updateable;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Will
 */
public class Scene implements Renderable, Updateable{
    public Collection<Renderable> renderObjects = new ArrayList<Renderable>();
    public Collection<Updateable> updateObjects = new ArrayList<Updateable>();
    
    public Scene() {
        
    }
    
     @Override
    public void render(int delta) {
        for(Renderable so : renderObjects) {
            so.render(delta);
        }
    }

    @Override
    public void update(float delta) {
        for(Updateable so : updateObjects) {
            so.update(delta);
        }
    }
    
    public void attach(SceneObject so) {
        if(so instanceof Renderable) {
            renderObjects.add((Renderable)so);
        }
        if(so instanceof Updateable) {
            updateObjects.add((Updateable)so);
        }
    }
    
    public void detach(SceneObject so) {
        if(so instanceof Renderable) {
            renderObjects.remove((Renderable)so);
        }
        if(so instanceof Updateable) {
            updateObjects.remove((Updateable)so);
        }
    }
}
