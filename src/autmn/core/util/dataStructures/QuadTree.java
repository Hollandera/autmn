/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autmn.core.util.dataStructures;

/**
 *
 * @author Will
 * @param <T>
 */
public class QuadTree<T> {
    public enum Cell {
        TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
    }
    
    private final int depth;
    private T data;
    private QuadTree<T> parent, topLeft, topRight, bottomLeft, bottomRight;
    
    public QuadTree(int depth) {
        this.depth = depth;
    }
    
    public void setCell(Cell cell, QuadTree<T> data) {
        switch(cell) {
            case TOP_LEFT:
                topLeft = data;
                break;
            case TOP_RIGHT:
                topRight = data;
                break;
            case BOTTOM_LEFT:
                bottomLeft = data;
                break;
            case BOTTOM_RIGHT:
                bottomRight = data;
                break;
        }
    }
    
    public void expand(Cell cell) {
        if(parent == null) {
            parent = new QuadTree<T>(depth + 1);
            parent.setCell(cell, this);
        } else {
            parent.expand(cell);
        }
    }
    
    public void setData(T data) {
        this.data = data;
    }
    
    public int getDepth() {
        return depth;
    }
}
