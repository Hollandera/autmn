/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autmn.core.collision;

import autmn.core.util.math.Vector2;

/**
 *
 * @author Will
 */
public class AABB implements Collidable{
    private Vector2 lowerBound, upperBound;
    
    public AABB(Vector2 lowerBound, Vector2 upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }
    
    public AABB translate(int x, int y) {
        lowerBound.add(x, y);
        upperBound.add(x, y);
        return this;
    }
    
    public AABB translate(Vector2 distance) {
        lowerBound.add(distance);
        upperBound.add(distance);
        return this;
    }
}
