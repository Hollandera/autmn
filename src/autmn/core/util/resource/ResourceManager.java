package autmn.core.util.resource;

import java.util.HashMap;

/**
 * 
 * @author william gervasio
 *
 */
public class ResourceManager {
	private static HashMap<Integer, Resource> resourceMap = new HashMap<Integer, Resource>();
	
	/**
	 * 
	 * @param id
	 * @param resource
	 */
	public static void add(int id, Resource resource) {
		resourceMap.put(id, resource);
	}
	
	/**
	 * 
	 * @param id
	 * @param type
	 * @return
	 */
	public static Resource get(int id, Class<Resource> type) {
		return resourceMap.get(id);
	}
	
	/**
	 * 
	 * @param id
	 * @param type
	 */
	public void remove(int id, Class<Resource> type) {
		resourceMap.remove(id);
	}
}
