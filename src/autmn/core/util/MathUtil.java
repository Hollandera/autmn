package autmn.core.util;

/**
 *
 * @author inferno04
 */
public class MathUtil {

    public static float ensureWithinRange(float value, float min, float max) {
        return value <= max ? value >= min ? value : min : max;
    }

}
