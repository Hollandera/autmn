/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autmn.core.graphics;

import autmn.core.util.resource.Resource;
import java.nio.ByteBuffer;

/**
 *
 * @author Will
 */
public class Image implements Resource {

    private int width, height;
    private ByteBuffer buffer;

    /**
     *
     * @param width
     * @param height
     * @param buffer
     */
    public Image(int width, int height, ByteBuffer buffer) {
        this.width = width;
        this.height = height;
        this.buffer = buffer;
    }

    /**
     *
     * @return The width of the image.
     */
    public int getWidth() {
        return width;
    }

    /**
     *
     * @return The height of the image.
     */
    public int getHeight() {
        return height;
    }

    /**
     *
     * @return The image buffer
     */
    public ByteBuffer getBuffer() {
        return buffer;
    }
    
}
